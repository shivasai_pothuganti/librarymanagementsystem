public class Book{

    public Book(){
        
    }

    public Book(int id,string name,string author,int noofcopies){

        this.bookId = id;
        this.bookName = name;
        this.bookAuthor = author;
        this.copiesAvailable = noofcopies;

    }

    public Book(string name,string author,int noofcopies){
        this.bookName = name;
        this.bookAuthor = author;
        this.copiesAvailable = noofcopies;
    }

    public int bookId {get;set;}
    public string bookName {get;set;}

    public string bookAuthor {get;set;}


    public int copiesAvailable{get;set;}

    public override string ToString()
    {
        return (this.bookId+" "+this.bookName+" "+this.bookAuthor+" "+this.copiesAvailable);
    }

}
