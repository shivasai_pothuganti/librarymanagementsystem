using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using library.Models;
using System.Data.SqlClient; 
using System.Collections.Generic;
using System;

namespace library.Controllers{
    public class LendBookController:Controller{
        List<Brrowed> history = new List<Brrowed>();
        private readonly ILogger<LendBookController> _logger;

        public LendBookController(ILogger<LendBookController> logger){
            _logger = logger;
        }

        public IActionResult Index(){
            return View();
        }

        public IActionResult lend(Lend lenddetails){
            Console.WriteLine(lenddetails.username);
            Console.WriteLine(lenddetails.bookid);
            try{
                SqlConnection con = new SqlConnection("Server=localhost;database=library;integrated security=false;User ID=SA;Password=Shivasai@123");
                string query = "SELECT userId FROM Users WHERE userName=@username";
                con.Open();
                    SqlCommand queryCmd = new SqlCommand(query,con);
                    queryCmd.Parameters.AddWithValue("@username",lenddetails.username);
                     SqlDataReader read = queryCmd.ExecuteReader();
                     int userId = -1;
                     while(read.Read()){
                        userId = (int)read["userId"];
                     }
                     read.Close();
                    if(userId!=-1){
                        string dt =  DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");
                        Console.WriteLine(dt);
                        string insert = "INSERT INTO Borrow(userId,bookId,expiryDate) Values(@userid,@bookid,@expiryDate)";
                        try{
                            SqlCommand inst = new SqlCommand(insert,con);
                            inst.Parameters.AddWithValue("@userid",userId);
                            inst.Parameters.AddWithValue("@bookid",lenddetails.bookid);
                            inst.Parameters.AddWithValue("@expiryDate",dt);
                            inst.ExecuteNonQuery();
                            try{
                                string update_query = "UPDATE Copies SET nocopies = nocopies-1 WHERE bookId=@bookid";

                                SqlCommand decreaseCount = new SqlCommand(update_query,con);
                                decreaseCount.Parameters.AddWithValue("@bookid",lenddetails.bookid);
                                decreaseCount.ExecuteNonQuery();

                            }
                            catch(Exception e){
                                Console.WriteLine("update not succesfull");
                            }
                            return RedirectToAction(nameof(redirectHistory));
                        }
                        catch(Exception e){
                            Console.WriteLine(e);
                            View("Index");
                        }
                    }
                    else{
                        return View("Index");
                    }
                con.Close();
            }
            catch(Exception e){
                Console.WriteLine("connection failed another exception");
            }
            return View("Index");
        }

        public IActionResult redirectHistory(){
            
            return View("History",history);
        }

        public IActionResult History(string username){
            try{
                SqlConnection con = new SqlConnection("Server=localhost;Database=library;Integrated security=false;User=SA;Password=Shivasai@123");
                con.Open();
                string query = "SELECT userId from Users WHERE userName = @username";
                SqlCommand queryCmd = new SqlCommand(query,con);
                    queryCmd.Parameters.AddWithValue("@username",username);
                     SqlDataReader read = queryCmd.ExecuteReader();
                     int userId = -1;
                     while(read.Read()){
                        userId = (int)read["userId"];
                     }
                     read.Close();
                     if(userId!=-1){
                        string historyQuery = "SELECT Borrow.borrowId,Borrow.bookId,Borrow.expiryDate,Book.bookName FROM Borrow,Book WHERE userId = @userid AND Borrow.bookId = Book.bookId;";
                        try{
                            SqlCommand historycmd = new SqlCommand(historyQuery,con);
                            historycmd.Parameters.AddWithValue("@userid",userId);
                            SqlDataReader historyReader = historycmd.ExecuteReader();
                             while(historyReader.Read()){
                                Brrowed new_borrow = new Brrowed(historyReader["bookName"].ToString(),historyReader["expiryDate"].ToString(),(int)historyReader["borrowId"]);
                                history.Add(new_borrow);
                            }
                        }
                        catch(Exception e){
                            Console.WriteLine("reader error");
                            Console.WriteLine(e);
                        }
                        return View("History",history);
                     }
                     else{
                        Console.WriteLine("user not found");
                     }
                con.Close();

            }
            catch(Exception e){
                Console.WriteLine(e);
                Console.WriteLine("connection failed");
            }

            Console.WriteLine(username);
            return View("History",history);
        }
    
        public IActionResult DeleteBook(int borrowid){
            
            try{
                SqlConnection con = new SqlConnection("Server=localhost; database=library; integrated security=false; User=SA; Password=Shivasai@123 ");
                con.Open();
                    try{
                        int bookid = -1;
                        try{  
                            SqlCommand cmd = new SqlCommand("SELECT bookId FROM Borrow where borrowId=@borrowid;",con);
                            cmd.Parameters.AddWithValue("@borrowid",borrowid);
                            SqlDataReader bookidreader = cmd.ExecuteReader();
                            while(bookidreader.Read()){
                                bookid = (int)bookidreader["bookId"];
                            }
                            bookidreader.Close();
                        }
                        catch(Exception newe){
                            Console.WriteLine("cant get bookid");
                        }
                        string delquery = "DELETE FROM Borrow WHERE borrowId=@borrowid";
                        SqlCommand delcmd = new SqlCommand(delquery,con);
                        delcmd.Parameters.AddWithValue("@borrowid",borrowid);
                        int res = delcmd.ExecuteNonQuery();
                        try{
                            string update_query = "UPDATE Copies SET nocopies = nocopies+1 WHERE bookId=@bookid";

                            SqlCommand decreaseCount = new SqlCommand(update_query,con);
                            decreaseCount.Parameters.AddWithValue("@bookid",bookid);
                            decreaseCount.ExecuteNonQuery();
                        }
                        catch(Exception juste){
                            Console.WriteLine("failed");
                        }
                        return RedirectToAction(nameof(redirectHistory));
                    }
                    catch(Exception aloe){
                        Console.WriteLine("deletion failed"+aloe);
                    }
                con.Close();
            }
            catch(Exception e){
                Console.WriteLine("connection failed");
            }
            return View("History",history);
        }
    }
}