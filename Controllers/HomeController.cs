﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using library.Models;

namespace library.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult control(string submit){

        switch(submit){
            case "lend a book":
                return RedirectToAction("Index","DisplayBooks",new{area=""});
            case "add a book":
                return RedirectToAction("Index","AddBook",new{area=""});
            default:
                return View("Index");
        }
    }

    

    public IActionResult Test(){
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
