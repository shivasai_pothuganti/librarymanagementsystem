using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using library.Models;
using System.Data.SqlClient;

namespace library.Controllers{
    public class AddBookController :Controller{
        private readonly ILogger<AddBookController> _logger;

        public AddBookController(ILogger<AddBookController> logger){
            _logger = logger;
        }

        public IActionResult Index(){
            return View();
        }

        public IActionResult addBook(Book new_book){
            try{
                SqlConnection con = new SqlConnection("Server=localhost;database=library;Integrated Security = false;User Id=SA;Password=Shivasai@123");
                con.Open();
                string insertCommand = "INSERT INTO Book(bookName,bookAuthor,copiesAvailable) OUTPUT INSERTED.bookId VALUES(@bookName,@bookAuthor,@copiesAvailable)";

                SqlCommand cmd = new SqlCommand(insertCommand,con);
                cmd.Parameters.AddWithValue("@bookName",new_book.bookName);
                cmd.Parameters.AddWithValue("@bookAuthor",new_book.bookAuthor);
                cmd.Parameters.AddWithValue("@copiesAvailable",new_book.copiesAvailable);
                try{
                    int id = (int)cmd.ExecuteScalar();
                    Console.WriteLine(id);
                    SqlCommand addCopies = new SqlCommand("INSERT INTO Copies(bookId,noCopies) VALUES(@bookid,@noCopies) ",con);
                    addCopies.Parameters.AddWithValue("@bookid",id);
                    addCopies.Parameters.AddWithValue("@noCopies",new_book.copiesAvailable);
                    try{
                        addCopies.ExecuteNonQuery();
                    }
                    catch(Exception e){
                        Console.WriteLine("did not update copies table");
                    }
                }
                catch(Exception e){
                    Console.WriteLine("did not id");
                    Console.WriteLine(e);
                }
                con.Close();

            }
            catch(Exception e){
                Console.WriteLine(e);
            }
            return RedirectToAction("Index","LendBook",new {area=""});
        }
        public IActionResult removeBook(string bookname){
            try{
                SqlConnection con = new SqlConnection("Server=localhost;database=library;Integrated Security = false;User Id=SA;Password=Shivasai@123");
                con.Open();

                string delquery = "DELETE FROM Book WHERE bookName=@bookName";

                Console.WriteLine(delquery);
                try{
                    SqlCommand delcmd = new SqlCommand(delquery,con);
                    delcmd.Parameters.AddWithValue("@bookName",bookname);
                    int res = delcmd.ExecuteNonQuery();
                    Console.WriteLine(res);
                }
                catch(Exception e){
                    Console.WriteLine("delete command error");
                    Console.WriteLine(e);
                }
                con.Close();
            }
            catch(Exception e){
                Console.WriteLine(e);
            }
            return RedirectToAction("Index","LendBook",new {area=""});
        }
        
    }
}