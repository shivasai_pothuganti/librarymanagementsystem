using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;

namespace library.Controllers {
    public class DisplayBooks : Controller{
    private readonly ILogger<DisplayBooks> _logger;

        public DisplayBooks(ILogger<DisplayBooks> logger){
            _logger = logger;
        }
    public IActionResult Index(){
            List<Book> books = new List<Book> ();
            try{
                SqlConnection con = new SqlConnection("Server=localhost; database=library; integrated security=false; User ID=SA; Password=Shivasai@123");
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT Book.bookId,Book.bookName,Book.bookAuthor,Copies.nocopies FROM Book,Copies where Book.bookId = Copies.bookId AND Copies.nocopies >0; ",con);
                SqlDataReader sdr = cmd.ExecuteReader();

                while(sdr.Read()){
                    //Console.WriteLine(sdr["bookId"]+" "+sdr["bookName"]+" "+sdr["bookAuthor"]+" "+sdr["copiesAvailable"]);
                    var new_book = new Book((int)sdr["bookId"],sdr["bookName"].ToString(),sdr["bookAuthor"].ToString(),(int)sdr["nocopies"]);
                    books.Add(new_book);
                }
                con.Close();
            }
            catch(Exception e){
                Console.WriteLine("error connecting the data base"+e);
            }

            return View(books);
        }
    public IActionResult lend(){
        return RedirectToAction("Index","LendBook",new{area=""});
    }
}   
}